#include "TrackSelector.hh"
#include "xAODJet/Jet.h"

TrackSelector::TrackSelector(TrackSelectorConfig config):
  m_track_associator("BTagTrackToJetAssociator"),
  m_track_select_cfg(config){}


TrackSelector::Tracks TrackSelector::get_tracks(const xAOD::Jet& jet) const
{
  const xAOD::BTagging *btagging = jet.btagging();
  std::vector<const xAOD::TrackParticle*> tracks;
  for (const auto &link : m_track_associator(*btagging)) {
    if(link.isValid()) {
      const xAOD::TrackParticle *tp = *link;
      if (passed_cuts(*tp)) {
        tracks.push_back(tp);
      }
    } else {
      throw std::logic_error("invalid track link");
    }
  }
  return tracks;
}

bool TrackSelector::passed_cuts(const xAOD::TrackParticle& tp) const
{
  static SG::AuxElement::ConstAccessor<float> d0("btagIp_d0");
  static SG::AuxElement::ConstAccessor<float> z0("btagIp_z0SinTheta");
  static SG::AuxElement::ConstAccessor<unsigned char> pix_hits("numberOfPixelHits");
  static SG::AuxElement::ConstAccessor<unsigned char> pix_holes("numberOfPixelHoles");
  static SG::AuxElement::ConstAccessor<unsigned char> pix_shared("numberOfPixelSharedHits");
  static SG::AuxElement::ConstAccessor<unsigned char> pix_dead("numberOfPixelDeadSensors");
  static SG::AuxElement::ConstAccessor<unsigned char> sct_hits("numberOfSCTHits");
  static SG::AuxElement::ConstAccessor<unsigned char> sct_holes("numberOfSCTHoles");
  static SG::AuxElement::ConstAccessor<unsigned char> sct_shared("numberOfSCTSharedHits");
  static SG::AuxElement::ConstAccessor<unsigned char> sct_dead("numberOfSCTDeadSensors");


  if (std::abs(tp.eta()) > 2.5)
          return false;
  double n_module_shared = (pix_shared(tp) + sct_shared(tp) / 2);
  if (n_module_shared > 1)
    return false;
  if (tp.pt() <= m_track_select_cfg.pt_minumum)
    return false;
  if (std::abs(d0(tp)) >= m_track_select_cfg.d0_maximum)
    return false;
  if (std::abs(z0(tp)) >= m_track_select_cfg.z0_maximum)
    return false;
  if (pix_hits(tp) + pix_dead(tp) + sct_hits(tp) + sct_dead(tp) < m_track_select_cfg.si_hits_minimum)
    return false;
  if ((pix_holes(tp) + sct_holes(tp)) > m_track_select_cfg.si_holes_maximum)
    return false;
  if (pix_holes(tp) > m_track_select_cfg.pix_holes_maximum)
    return false;
  return true;
}


