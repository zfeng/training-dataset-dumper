#include "DecoratorExample.hh"
#include "TrackSelector.hh"


// the constructor just builds the decorator
DecoratorExample::DecoratorExample(const std::string& prefix):
  m_deco(prefix + "decorator")
  //m_M_D_reco(prefix + "M_D_reco"),
  //m_dM_DK_reco(prefix + "dM_DK_reco"),
  //m_M2_pipi_reco(prefix + "M2_pipi_reco"),
  //m_M2_Kpi_reco(prefix + "M2_Kpi_reco")
{
}
bool DecoratorExample::particleInCollection(const xAOD::TrackParticle *trkPart, const std::vector< ElementLink< xAOD::TrackParticleContainer > > trkColl ) const {
  for (unsigned int iT = 0; iT < trkColl.size(); iT++) {
      if (trkPart == *(trkColl.at(iT))) return true;
  }
  return false;
}

// this call actually does the work on the jet
void DecoratorExample::decorate(const xAOD::Jet& jet) const {
  // it's good practice to check that the b-tagging object exists. It
  // _should_ in most cases, but if not better to avoid the segfault.
  const xAOD::BTagging* btag = jet.btagging();
  if (!btag) {
    throw std::runtime_error("no b-tagging object on jet");
  }

  // Store something to the jet.

  // Get the JF info
  float jfm;
  int Nvtx = 0;
  int jfntrkAtVx;
  jfm = -99;
  jfntrkAtVx = -1;
  btag->taggerInfo(Nvtx, xAOD::JetFitter_nVTX); // JF nVTX
  if (Nvtx>0){
  	btag->taggerInfo(jfm, xAOD::JetFitter_mass); // JF jet mass, as default value
	btag->taggerInfo(jfntrkAtVx, xAOD::BTagInfo::JetFitter_nTracksAtVtx); // JF nTrks
  } else {
  	m_deco(*btag) = jfm;
  }

  // trying to find out secondary vertex by myself
  std::vector< ElementLink< xAOD::TrackParticleContainer > > JFTracks;
  std::vector<float> fittedPosition;
  try{
  	fittedPosition = btag->auxdata<std::vector<float>>("JetFitter_fittedPosition");
  } catch(SG::ExcBadAuxVar& exc) {
  	std::cout << " missing JetFitter_fittedPosition " << std::endl;
  }

  std::vector<float> fittedCov;
  try{
  	fittedCov = bjet->auxdata<std::vector<float> >("JetFitter_fittedCov");
  } catch(SG::ExcBadAuxVar& exc) {
	std::cout << " missing JetFitter_fittedCov " << std::endl;
  }
  
  
  //  Vertices & Tracks
  std::vector< ElementLink<xAOD::BTagVertexContainer> > jfvertices;
  try {
  	jfvertices =  btag->auxdata<std::vector<ElementLink<xAOD::BTagVertexContainer> > >("JetFitter_JFvertices");
  } catch (...) {};

  int SV_index = -99;
  int TV_index = -99;
  float closestVtx_L3D = -10;
  for (unsigned int jfv = 0; jfv<jfvertices.size(); jfv++){
  	if (!jfvertices.at(jfv).isValid()) continue;

	const xAOD::BTagVertex *tmpVertex = *(jfvertices.at(jfv));
	const std::vector< ElementLink<xAOD::TrackParticleContainer> > tmpVect = tmpVertex->track_links(); // mod Remco
    JFTracks.insert(JFTracks.end(), tmpVect.begin(), tmpVect.end());
	if (jfv < fittedPosition.size()-5) {
		float jf_vtx_L3d = fittedPosition[jfv+5];
		if (jf_vtx_L3d > 0) {
			if (jf_vtx_L3d < closestVtx_L3D || closestVtx_L3D < 0) {
				closest_Vtx_L3D = jf_vtx_L3d;
				SV_index = jfv;
			}
		}
	} 
  } // now SV_index is the index of the SV
  
  // track loop
  std::vector<ElementLink<xAOD::TrackParticleContainer>> assocTracks;
  std::vector<const xAOD::TrackParticle*> selectedTracks; // tracks passing number of Pixel and SCT hits requirements
  assocTracks = btag->auxdata<std::vector <ElementLink <xAOD::TrackParticleContainer> > >("BTagTrackToJetAssociator");


  if (Nvtx>1) {
    m_deco(*btag) = jfm;
  } else if (Nvtx==1){
  	// following only for Nvtx == 1 !!!
  
  	//track loop, select only tracks with 2 or more hits
  	uint8_t getInt(0);   // for accessing summary information
  	for (unsigned int iT = 0; iT < assocTracks.size(); iT++) {
  		if (!assocTracks.at(iT).isValid()) continue;
    
		const xAOD::TrackParticle *tmpTrk = *(assocTracks.at(iT));
    	tmpTrk->summaryValue(getInt, xAOD::numberOfPixelHits);
    
		int nSi = getInt;
		tmpTrk->summaryValue(getInt, xAOD::numberOfSCTHits);
		nSi += getInt;
    	if (nSi < 2) continue;

    	selectedTracks.push_back(tmpTrk);
  	}

  	// track loop variables
  	std::vector<int> j_trk_jf_Vertex;
  	std::vector<float> j_trk_jf_pt;
  	std::vector<float> j_trk_charge;
	std::vector<float> j_trk_jf_eta;
	std::vector<float> j_trk_jf_phi;

  	int trkIndex = 0;
  	float tot_charge = 0.0;

  	// track loop for the first time
  	for (const auto* tmpTrk: selectedTracks) {
    	int myVtx = -1;    

		for (unsigned int jfv = 0; jfv < jfvertices.size(); jfv++) { // mod Remco          
			if (!jfvertices.at(jfv).isValid()) continue;        
		
			const xAOD::BTagVertex *tmpVertex = *(jfvertices.at(jfv));
			const std::vector< ElementLink<xAOD::TrackParticleContainer> > tmpVect = tmpVertex->track_links();
	    	if (particleInCollection(tmpTrk, tmpVect)) myVtx = jfv;
		}
	
		j_trk_jf_Vertex.push_back(myVtx);
		j_trk_jf_pt.push_back(tmpTrk->pt());
		j_trk_charge.push_back(tmpTrk->charge());
	    j_trk_jf_eta.push_back(tmpTrk->eta());
		j_trk_jf_phi.push_back(tmpTrk->phi());
		// TLorentzVector trk;
		// trk.SetPtEtaPhiM(tmpTrk->pt(), tmpTrk->eta(), tmpTrk->phi(), track_mass);
		tot_charge += tmpTrk->charge();
		trkIndex++;	
  	} // end track loop

	std::vector<TLorentzVector> trk_Lorentz;
	TLorentzVector tracksTot4Mom(0,0,0,0);
	// jfntrkAtVx
    if (j_trk_jf_Vertex.size() != 3) {
		m_deco(*btag) = jfm;
	} else if (fabs(tot_charge) != 1) {
		m_deco(*btag) = jfm;
	} else{
  		for (unsigned int trk_jf = 0; trk_jf < j_trk_jf_Vertex.size(); trk_jf++){

			TLorentzVector tmptrk_Lorentz;
			float tmpCharge = j_trk_charge.at(trk_jf);
			
			if ((tmpCharge==1 && tot_charge==1)||(tmpCharge==-1 && tot_charge==-1)){
				tmptrk_Lorentz.SetPtEtaPhiM(j_trk_jf_pt.at(trk_jf), j_trk_jf_eta.at(trk_jf), j_trk_jf_phi.at(trk_jf), 139.57);
			} else if ((tmpCharge==1 && tot_charge==-1)||(tmpCharge==-1 && tot_charge==1)){
				tmptrk_Lorentz.SetPtEtaPhiM(j_trk_jf_pt.at(trk_jf), j_trk_jf_eta.at(trk_jf), j_trk_jf_phi.at(trk_jf), 493.677);
			} 
			trk_Lorentz.push_back(tmptrk_Lorentz);
			tracksTot4Mom += tmptrk_Lorentz;
		}
		float D_recon_mass;
		D_recon_mass = tracksTot4Mom.M();
		m_deco(*btag) = D_recon_mass;
	}
  } 

}
