#ifndef DECORATOR_EXAMPLE_HH
#define DECORATOR_EXAMPLE_HH

#include "xAODJet/Jet.h"

#include <string>

// this is a bare-bones example of a class that could be used to
// decorate a jet with additional information

class DecoratorExample
{
public:
  DecoratorExample(const std::string& decorator_prefix = "example_");

  // this is the function that actually does the decoration
  void decorate(const xAOD::Jet& jet) const;
private:
  // All this class does is apply a decoration, so all it needs to do
  // is contain one decorator. We could have also written a function
  // and statically initalized this, but static data in functions is
  // probably best avoided.
  SG::AuxElement::Decorator<float> m_deco;
  //SG::AuxElement::Decorator<float> m_M_D_reco;
  //SG::AuxElement::Decorator<float> m_dM_DK_reco;
  //SG::AuxElement::Decorator<float> m_M2_pipi_reco;
  //SG::AuxElement::Decorator<float> m_M2_Kpi_reco;
  bool particleInCollection(const xAOD::TrackParticle *trkPart, const std::vector< ElementLink< xAOD::TrackParticleContainer > > trkColl ) const;
};

#endif
