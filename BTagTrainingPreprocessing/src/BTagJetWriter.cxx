#include "BTagJetWriter.hh"
#include "BTagJetWriterConfig.hh"
#include "HDF5Utils/Writer.h"

// Less Standard Libraries (for atlas)
#include "H5Cpp.h"

// ATLAS things
#include "xAODJet/Jet.h"
#include "xAODEventInfo/EventInfo.h"

JetOutputs::JetOutputs():
  jet(nullptr),
  parent(nullptr),
  event_info(nullptr)
{}

typedef std::function<float(const JetOutputs&)> FloatFiller;
class JetConsumers: public H5Utils::Consumers<const JetOutputs&> {};
class JetOutputWriter: public H5Utils::Writer<0,const JetOutputs&>
{
public:
  JetOutputWriter(H5::Group& file,
                  const std::string& name,
                  const JetConsumers& cons):
    H5Utils::Writer<0,const JetOutputs&>(file, name, cons) {}
};


struct SubstructureAccessors {
  typedef SG::AuxElement AE;
  SubstructureAccessors();
  float c2(const xAOD::Jet&);
  float d2(const xAOD::Jet&);
  float e3(const xAOD::Jet&);
  float tau21(const xAOD::Jet&);
  float tau32(const xAOD::Jet&);
  float fw20(const xAOD::Jet&);

  AE::ConstAccessor<float> ecf1;
  AE::ConstAccessor<float> ecf2;
  AE::ConstAccessor<float> ecf3;

  AE::ConstAccessor<float> tau1wta;
  AE::ConstAccessor<float> tau2wta;
  AE::ConstAccessor<float> tau3wta;

  AE::ConstAccessor<float> fw2;
  AE::ConstAccessor<float> fw0;
};

SubstructureAccessors::SubstructureAccessors():
  ecf1("ECF1"), ecf2("ECF2"), ecf3("ECF3"),
  tau1wta("Tau1_wta"), tau2wta("Tau2_wta"), tau3wta("Tau3_wta"),
  fw2("FoxWolfram2"), fw0("FoxWolfram0")
{}
float SubstructureAccessors::c2(const xAOD::Jet& j) {
  return ecf3(j) * ecf1(j) / pow(ecf2(j), 2.0);
}
float SubstructureAccessors::d2(const xAOD::Jet& j) {
  return ecf3(j) * pow(ecf1(j), 3.0) / pow(ecf2(j), 3.0);
}
float SubstructureAccessors::e3(const xAOD::Jet& j) {
  return ecf3(j)/pow(ecf1(j),3.0);
}
float SubstructureAccessors::tau21(const xAOD::Jet& j) {
  return tau2wta(j) / tau1wta(j);
}
float SubstructureAccessors::tau32(const xAOD::Jet& j) {
  return tau3wta(j) / tau2wta(j);
}
float SubstructureAccessors::fw20(const xAOD::Jet& j) {
  return fw2(j) / fw0(j);
}

BTagJetWriter::BTagJetWriter(
  H5::Group& output_file,
  const BTagJetWriterConfig& config):
  m_write_parent(config.write_kinematics_relative_to_parent ||
                 config.parent_jet_int_variables.size() > 0),
  m_ssa(new SubstructureAccessors)
{ 
  // create the variable fillers
  JetConsumers fillers;
  if ( (config.double_variables.size()
        + config.float_variables.size()
        + config.int_variables.size() ) > 0) {
        // in this case we convert doubles to floats to save space,
        // note that you can change the second parameter to a double
        // to get full precision.
    const BTagVariableMaps& m = config.variable_maps;
    add_btag_fillers<double, float>(fillers, config.double_variables, m, NAN);
    add_btag_fillers<float>(fillers, config.float_variables, m, NAN);
    add_btag_fillers<int>(fillers, config.int_variables, m, -1);
    add_btag_fillers<char>(fillers, config.char_variables, m, -1);
    add_btag_fillers<int, float>(
      fillers, config.int_as_float_variables, m, NAN);
  }
  if (config.jet_float_variables.size() > 0) {
    add_jet_fillers<float>(fillers, config.jet_float_variables, NAN);
  }
  add_jet_int_variables(fillers, config.jet_int_variables);


  // some things like 4 momenta have to be hand coded
  FloatFiller pt = [](const JetOutputs& jo) -> float {
    if (!jo.jet) return NAN;
    return jo.jet->pt();
  };
  fillers.add("pt", pt);

  FloatFiller eta = [](const JetOutputs& jo) -> float {
    if (!jo.jet) return NAN;
    return jo.jet->eta();
  };
  fillers.add("eta", eta);

  if (m_write_parent) {
    if (config.write_kinematics_relative_to_parent) {
      add_parent_fillers(fillers);
    }
    if (config.parent_jet_int_variables.size() > 0) {
      add_parent_jet_int_variables(fillers, config.parent_jet_int_variables);
    }
  }

  if (config.write_event_info) add_event_info(fillers);
  if (config.write_substructure_moments) add_substructure(fillers);

  // now create the writer
  m_hdf5_jet_writer = new JetOutputWriter(output_file, config.name, fillers);
}

BTagJetWriter::~BTagJetWriter() {
  if (m_hdf5_jet_writer) m_hdf5_jet_writer->flush();
  delete m_hdf5_jet_writer;
  delete m_ssa;
}

BTagJetWriter::BTagJetWriter(BTagJetWriter&& old):
  m_write_parent(old.m_write_parent),
  m_hdf5_jet_writer(old.m_hdf5_jet_writer),
  m_ssa(old.m_ssa)
{
  old.m_hdf5_jet_writer = nullptr;
  old.m_ssa = nullptr;
}

void BTagJetWriter::write(const xAOD::Jet& jet,
                          const xAOD::EventInfo* mc_event_info) {
  if (m_write_parent) throw std::logic_error("misconfiguration");
  JetOutputs jo;
  jo.event_info = mc_event_info;
  jo.jet = &jet;
  m_hdf5_jet_writer->fill(jo);
}

void BTagJetWriter::write_dummy() {
  JetOutputs dummy;
  m_hdf5_jet_writer->fill(dummy);
}

void BTagJetWriter::write_with_parent(const xAOD::Jet& jet,
                                      const xAOD::Jet& parent,
                                      const xAOD::EventInfo* mc_event_info) {
  if (!m_write_parent) throw std::logic_error("misconfiguration");
  JetOutputs jo;
  jo.event_info = mc_event_info;
  jo.jet = &jet;
  jo.parent = &parent;
  m_hdf5_jet_writer->fill(jo);
}


template<typename I, typename O>
void BTagJetWriter::add_btag_fillers(
  JetConsumers& vars,
  const std::vector<std::string>& names,
  const BTagVariableMaps& maps,
  O default_value) {
  const auto& def_check_names = maps.replace_with_defaults_checks;
  for (const auto& btag_var: names) {
    SG::AuxElement::ConstAccessor<I> getter(btag_var);
    std::string out_name = btag_var;
    if (maps.rename.count(btag_var)) out_name = maps.rename.at(btag_var);
    if (def_check_names.count(btag_var)) {
      SG::AuxElement::ConstAccessor<char> def_check(
        def_check_names.at(btag_var));
      std::function<O(const JetOutputs&)> filler = [
        getter, default_value, def_check](const JetOutputs& jo) -> O {
        if (!jo.jet) return default_value;
        auto* btagging = jo.jet->btagging();
        if (!btagging) {
          throw std::logic_error("missing b-tagging container");
        }
        if (def_check(*btagging)) return default_value;
        return getter(*btagging);
      };
      vars.add(out_name, filler);
    } else {
      std::function<O(const JetOutputs&)> filler = [
        getter, default_value](const JetOutputs& jo) -> O {
        if (!jo.jet) return default_value;
        auto* btagging = jo.jet->btagging();
        if (!btagging) {
          throw std::logic_error("missing b-tagging container");
        }
        return getter(*btagging);
      };
      vars.add(out_name, filler);
    }
  }
}
template<typename I, typename O>
void BTagJetWriter::add_jet_fillers(JetConsumers& vars,
                                    const std::vector<std::string>& names,
                                    O default_value) {
  for (const auto& btag_var: names) {
    SG::AuxElement::ConstAccessor<I> getter(btag_var);
    std::function<O(const JetOutputs&)> filler = [
      getter, default_value](const JetOutputs& jo) -> O {
      if (!jo.jet) return default_value;
      return getter(*jo.jet);
    };
    vars.add(btag_var, filler);
  }
}

void BTagJetWriter::add_jet_int_variables(JetConsumers& vars,
                                     const std::vector<std::string>& names) {
  for (const auto& label_name: names) {
    // accomidate some custom cases for jet ints
    if (label_name == "numConstituents") {
      auto f = [](const JetOutputs& jo) -> int {
                 if (!jo.jet) return -1;
                 return jo.jet->numConstituents();
               };
      vars.add<int>(label_name, f);
    } else {
      // if nothing else works, we end up here
      SG::AuxElement::ConstAccessor<int> lab(label_name);
      std::function<int(const JetOutputs&)> filler
        = [lab](const JetOutputs& jo) {
            if (!jo.jet) return -1;
            return lab(*jo.jet);
          };
      vars.add(label_name, filler);
    }
  }
}
void BTagJetWriter::add_parent_jet_int_variables(
  JetConsumers& vars,
  const std::vector<std::string>& names) {
  for (const auto& label_name: names) {
    SG::AuxElement::ConstAccessor<int> lab(label_name);
    std::function<int(const JetOutputs&)> filler = [
      lab](const JetOutputs& jo) {
      if (!jo.parent) return -1;
      return lab(*jo.parent);
    };
    vars.add(label_name, filler);
  }
}

void BTagJetWriter::add_parent_fillers(JetConsumers& vars) {
  FloatFiller deta = [](const JetOutputs& jo) -> float {
    if (!jo.jet || !jo.parent) return NAN;
    return jo.jet->eta() - jo.parent->eta();
  };
  vars.add("deta", deta);
  FloatFiller dphi = [](const JetOutputs& jo) -> float {
    if (!jo.jet || !jo.parent) return NAN;
    return jo.jet->p4().DeltaPhi(jo.parent->p4());
  };
  vars.add("dphi", dphi);
  FloatFiller dr = [](const JetOutputs& jo) -> float {
    if (!jo.jet || !jo.parent) return NAN;
    return jo.jet->p4().DeltaR(jo.parent->p4());
  };
  vars.add("dr", dr);
}

void BTagJetWriter::add_substructure(JetConsumers& vars) {
  FloatFiller mass = [](const JetOutputs& jo) -> float {
    if (!jo.jet) return NAN;
    return jo.jet->m();
  };
  vars.add("mass", mass);

  FloatFiller c2 = [ssa=m_ssa](const JetOutputs& jo) -> float {
    if (!jo.jet) return NAN;
    return ssa->c2(*jo.jet);
  };
  vars.add("C2", c2);

  FloatFiller d2 = [ssa=m_ssa](const JetOutputs& jo) -> float {
    if (!jo.jet) return NAN;
    return ssa->d2(*jo.jet);
  };
  vars.add("D2", d2);

  FloatFiller e3 = [ssa=m_ssa](const JetOutputs& jo) -> float {
    if (!jo.jet) return NAN;
    return ssa->e3(*jo.jet);
  };
  vars.add("e3", e3);

  FloatFiller tau21 = [ssa=m_ssa](const JetOutputs& jo) -> float {
    if (!jo.jet) return NAN;
    return ssa->tau21(*jo.jet);
  };
  vars.add("Tau21_wta", tau21);

  FloatFiller tau32 = [ssa=m_ssa](const JetOutputs& jo) -> float {
    if (!jo.jet) return NAN;
    return ssa->tau32(*jo.jet);
  };
  vars.add("Tau32_wta", tau32);

  FloatFiller fw20 = [ssa=m_ssa](const JetOutputs& jo) -> float {
    if (!jo.jet) return NAN;
    return ssa->fw20(*jo.jet);
  };
  vars.add("FoxWolfram20", fw20);

}

void BTagJetWriter::add_event_info(JetConsumers& vars) {
  std::function<float(const JetOutputs&)> evt_weight = [](const JetOutputs& jo) {
    if (!jo.event_info) throw std::logic_error("missing event info");
    if (jo.event_info->eventType(xAOD::EventInfo::IS_SIMULATION)) {
      return jo.event_info->mcEventWeight();
    }
    else {return float(1.0);}
  };
  std::function<long long(const JetOutputs&)> evt_number = [](const JetOutputs& jo) {
    if (!jo.event_info) throw std::logic_error("missing event info");
    return jo.event_info->eventNumber();
  };
  std::function<long long(const JetOutputs&)> run_number = [](const JetOutputs& jo) {
    if (!jo.event_info) throw std::logic_error("missing event info");
    return jo.event_info->runNumber();
  };

  std::function<float(const JetOutputs&)> evt_mu = [](const JetOutputs& jo) {
    if (!jo.event_info) throw std::logic_error("missing event info");
    return jo.event_info->averageInteractionsPerCrossing();
  };

  std::function<float(const JetOutputs&)> evt_act_mu = [](const JetOutputs& jo) {
    if (!jo.event_info) throw std::logic_error("missing event info");
    return jo.event_info->actualInteractionsPerCrossing();
  };

  vars.add("mcEventWeight", evt_weight);
  vars.add("eventNumber", evt_number);
  vars.add("runNumber", run_number);
  vars.add("averageInteractionsPerCrossing", evt_mu);
  vars.add("actualInteractionsPerCrossing", evt_act_mu);
}
