#include "BTagTrackWriter.hh"
#include "BTagTrackWriterConfig.hh"
#include "HDF5Utils/Writer.h"

// Less Standard Libraries (for atlas)
#include "H5Cpp.h"

// ATLAS things
#include "xAODTracking/TrackParticle.h"
#include "xAODJet/Jet.h"

#include "FlavorTagDiscriminants/customGetter.h"

/////////////////////////////////////////////
// Internal classes
/////////////////////////////////////////////

typedef std::function<float(const TrackOutputs&)> FloatFiller;
class TrackConsumers: public H5Utils::Consumers<const TrackOutputs&> {};
class TrackOutputWriter: public H5Utils::Writer<1,const TrackOutputs&>
{
public:
  TrackOutputWriter(H5::Group& file,
                    const std::string& name,
                    const TrackConsumers& cons,
                    size_t size):
    H5Utils::Writer<1,const TrackOutputs&>(file, name, cons, {{size}}) {}
};

// wrapper class to convert the things we have in
// FlavorTagDiscriminants into something that HDF5Utils can read.
template <typename T>
class CustomSeqWrapper
{
  using SeqGetter = decltype(
    FlavorTagDiscriminants::customSequenceGetter(std::declval<std::string>()));
public:
  CustomSeqWrapper(SeqGetter g): m_getter(g) {
  }
  T operator()(const TrackOutputs& t) {
    auto output_vec = m_getter(*t.jet, {t.track});
    return output_vec.at(0);
  }
private:
  SeqGetter m_getter;
};

///////////////////////////////////
// Class definition starts here
///////////////////////////////////
BTagTrackWriter::BTagTrackWriter(
  H5::Group& output_file,
  const BTagTrackWriterConfig& config):
  m_hdf5_track_writer(nullptr)
{

  TrackConsumers fillers;
  // add the variables passed as strings
  add_track_fillers<float>(fillers, config.float_variables, NAN);
  // We can convert the doubles to floats to save space. Change the
  // float below to a double if you want to save with full precision.
  add_track_fillers<double, float>(fillers, config.double_variables, NAN);
  add_track_fillers<int>(fillers, config.int_variables, -1);
  add_track_fillers<unsigned char, unsigned char>(fillers, config.uchar_variables, 0);

  // use custom variables from the b-tagging inference code in
  // FlavorTagDiscriminants
  for (const std::string& name: config.flavortagdiscriminants_sequences) {
    auto getter = FlavorTagDiscriminants::customSequenceGetter(name);
    // note that within FlavorTagDiscriminants all the floats are
    // double we're truncating here because that precision is
    // probably not needed
    CustomSeqWrapper<float> wrapped(getter);
    fillers.add(name, std::function(wrapped));
  }

  add_rel_jet_kinematics(fillers);

  // build the output dataset
  assert(config.name.size() > 0);
  assert(config.output_size.size() == 1);
  std::vector<hsize_t> size(config.output_size.begin(),
                            config.output_size.end());
  if (config.output_size.at(0) > 0) {
    m_hdf5_track_writer = new TrackOutputWriter(output_file, config.name,
                                                fillers, size.at(0));
  }
}

BTagTrackWriter::~BTagTrackWriter() {
  if (m_hdf5_track_writer) m_hdf5_track_writer->flush();
  delete m_hdf5_track_writer;
}

void BTagTrackWriter::write(const BTagTrackWriter::Tracks& tracks,
                            const xAOD::Jet& jet) {
  if (m_hdf5_track_writer) {
    std::vector<TrackOutputs> trk_outputs;
    for (const auto* trk: tracks) {
      trk_outputs.push_back(TrackOutputs{trk, &jet});
    }
    m_hdf5_track_writer->fill(trk_outputs);
  }
}
void BTagTrackWriter::write_dummy() {
  if (m_hdf5_track_writer) {
    std::vector<TrackOutputs> trk_outputs;
    m_hdf5_track_writer->fill(trk_outputs);
  }
}

template<typename I, typename O>
void BTagTrackWriter::add_track_fillers(TrackConsumers& vars,
                                      const std::vector<std::string>& names,
                                      O def_value) {

  for (const auto& btag_var: names) {
    SG::AuxElement::ConstAccessor<I> getter(btag_var);
    std::function<O(const TrackOutputs&)> filler =
      [getter](const TrackOutputs& trk) -> O {
        return getter(*trk.track);
      };
    vars.add(btag_var, filler, def_value);
  }
}

void BTagTrackWriter::add_rel_jet_kinematics(TrackConsumers& vars) {

  FloatFiller deta = [](const TrackOutputs& trk) -> float {
    return trk.track->eta() - trk.jet->eta();
  };
  vars.add("deta", deta, NAN);

  FloatFiller dphi = [](const TrackOutputs& trk) -> float {
    return trk.track->p4().DeltaPhi(trk.jet->p4());
  };
  vars.add("dphi", dphi, NAN);

  FloatFiller dr = [](const TrackOutputs& trk) -> float {
    return trk.track->p4().DeltaR(trk.jet->p4());
  };
  vars.add("dr", dr, NAN);

  FloatFiller ptfrac = [](const TrackOutputs& trk) -> float {
    return trk.track->pt() / trk.jet->pt();
  };
  vars.add("ptfrac", ptfrac, NAN);

}
