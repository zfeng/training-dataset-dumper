#ifndef HBB_CONFIG_HH
#define HBB_CONFIG_HH

#include "DL2Config.hh"

#include <string>
#include <vector>
#include <map>

struct SubjetConfig {
  std::string input_name;
  std::string output_name;
  bool get_subjets_from_parent;
  size_t n_subjets_to_save;
  size_t n_tracks;
  size_t num_const;
  double min_jet_pt;
  std::map<std::string,std::vector<std::string>> variables;
  std::vector<DL2Config> dl2_configs;
  bool skip_if_missing_tracks;
};



struct HbbConfig {
  std::vector<SubjetConfig> subjet_configs;
  std::string jet_collection;
  std::string jet_calib_file;
  std::string cal_seq;
  std::string cal_area;
  size_t n_const;
  std::string top_tag_config;
  std::vector<std::string> grl_config; 
  std::vector<std::string> trig_config; 
  std::vector<std::string> hbb_tag_config;
  std::map<std::string,std::vector<std::string>> variables;
  std::string truth_wz_electron_container;
};

HbbConfig get_hbb_config(const std::string& config_file_name);

#endif
